from __future__ import print_function

import traceback

import telebot
import time
import pprint as pp
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
from telebot import types
import ast
import time
import telebot
import os
bot_token = '5054821553:AAFlu54bmkCjsIuqvLqv5nNuTaSOmZBujfc'
bot = telebot.TeleBot(token=bot_token)
opciones = {"/primero": "Primer código", "/segundo": "Segundo código", "/tercero": "Tercer código", "/cuarto": "Cuarto código", "/quinto": "Quinto código", "/ayuda": "Ayuda!"}

print("Servidor iniciado!")

def makeKeyboard(stringList):

    markup = types.InlineKeyboardMarkup()

    for key, value in stringList.items():
        markup.add(types.InlineKeyboardButton(text=value, callback_data="['value', '" + value + "', '" + key + "']"))

    return markup


@bot.message_handler(commands=['hola', 'start'])
def send_welcome(message):
    bot.send_message(message.chat.id, '¡Hola Naira! \nBienvenida a esta aventura!\n, llevo tiempo esperándote.\nLa pirata japonesa Oni, a finales del siglo 19, se vió cercada en la ciudad de Granada por unos perros salchicha. La astuta corsaria tuvo que renunciar a su más preciado tesoro. Pero, para que éste no cayera en manos equivocadas, decidió esconderlo en algún recóndito lugar de la Pachamama. Nadie ha sido capaz de encontrar su alijo hasta día de hoy.\n\n¿Serás capaz? ')
    opciones_solo_codigo_1 = {"/primero": "Desvelar primer enigma"}
    bot.send_message(chat_id=message.chat.id,
                     text="Estos son los códigos que necesitamos descifrar!",
                     reply_markup=makeKeyboard(opciones_solo_codigo_1),
                     parse_mode='HTML')


@bot.message_handler(commands=['primera'])
def first_clue(message):
    bot.reply_to(message, "Nos ha costado 5 jerseys de ¡¡Kachemir!! acceder a la sala de códigos.\n\n"
                          "Hemos descubierto que la ubicación del tesoro está conectada de alguna forma a una compuerta en salobreña. Esta compuerta solo se podrá activar cuando introduzcamos desde Granada los códigos que Oni cifró.\n\n"
                          "El primer enigma no conseguimos resolverlo, dice así:\n\nHay algo que, aunque te pertenezca, la gente siempre lo utiliza más que tú. ¿Qué es?")


@bot.message_handler(commands=['segundo'])
def second_clue(message):
    bot.reply_to(message, "Oni es demasiado ingeniosa como para símplemente esconder su tesoro, pero esta vez la vamos a sorprender!\n\n"
                            "El siguiente enigma, son un conjunto de números y letras que tienen que significar alguna palabra.\n\n0gr0d3145M0nt4N|45")


@bot.message_handler(commands=['tercero'])
def third_clue(message):
    bot.reply_to(message, "La siguiente adivinanza parece muy fácil, dice así:\n\n"
                          "Es el rey de los alimentos con agua, al que todos los gatos tememos.\n\n"
                          "Alimentos con mucha agua pueden ser algunos como la sandía, pero ¿Qué los gatos teman?\n")


@bot.message_handler(commands=['cuarto'])
def fourth_clue(message):
    bot.reply_to(message, "Vamos a muy buen ritmo, pero no hay que confiarse.\n\n"
                          "Tienes que enviarme una imagen. Nosotros tenemos una imagen antigua de Oni, pero necesitamos una foto de su cara muy cerca en la que se vean bien los ojos para que pase el exame de huella ocular!!\n\n"
                          "¡Encuéntrala y envíame esa imagen! \n")


@bot.message_handler(commands=['quinto'])
def fifth_clue(message):
    bot.send_message(message.chat.id, "¡La chiquinemiga abre la puerta!\n\n"
                          "¡Tienes que encontrar una chiquinemiga para abrir la puerta! \n\n"
                          "Nuestro equipo está investigando pero no consigue encontrar que es una chiquinemiga \n\nNecesitamos que nos envíes una foto de ella.")


@bot.message_handler(commands=['nota'])
def read_note(message):
    bot.reply_to(message, "En la nota solo dice\n\n    ...   Para Elisa    ...     \n\nNo entendemos nada, ponte en contacto con nuestro espía encubierto\n\n"
                          "Tiene dos nombres en clave, uno es jose ramón y cajón, y el otro es maridito.\n\nPonte en contacto con el urgentemente e intenta encontrar el tesoro!")


@bot.message_handler(commands=['test'])
def handle_command_adminwindow(message):
    bot.send_message(chat_id=message.chat.id,
                     text="Here are the values of stringList",
                     reply_markup=makeKeyboard(opciones),
                     parse_mode='HTML')


# Handle all other messages with content_type 'text' (content_types defaults to ['text'])
@bot.message_handler(func=lambda message: True)
def echo_message(message):
    if "onichi" in message.text or "Onichi" in message.text:
        bot.reply_to(message, message.text+"!!!!!!!!!!!")
        bot.send_message(message.chat.id, "Exacto!!\nEsa era la respuesta, con que su nombre real no es manchita sino ¡Ogro de las montañas!\nEra de esperar."
                                               "\n\nPerfecto, podemos pasar al siguiente código!")

        opciones_solo_codigo_3 = {"/tercero": "Tercer código"}
        bot.send_message(chat_id=message.chat.id,
                         text="Desvelar siguiente enigma",
                         reply_markup=makeKeyboard(opciones_solo_codigo_3),
                         parse_mode='HTML')
    if "nombre" in message.text or "Nombre" in message.text:
        bot.reply_to(message, "Claro, es el nombre!!!!!!!!!!!")
        bot.send_message(message.chat.id, "Exacto!!\nSe ha abierto una puerta!\nHa apareciddo el siguiente acertijo.")

        second_clue(message)

    if "pepino" in message.text or "Pepino" in message.text:
        bot.reply_to(message, "Vaya, con que el pepino!!!!")
        bot.send_message(message.chat.id,
                         "Quién iba a decir que sería algo tan fácil!!\n\nQue raro!!\nHan aparecido dos mirillas, parece que sirven para escanear las retinas.")
        fourth_clue(message)

@bot.message_handler(content_types=['photo'])
def photo(message):
    print('message.photo =', message.photo)
    fileID = message.photo[-1].file_id
    print('fileID =', fileID)
    file_info = bot.get_file(fileID)
    print('file.file_path =', file_info.file_path)
    downloaded_file = bot.download_file(file_info.file_path)

    os.makedirs(os.path.dirname(os.getcwd()+"/photos/"), exist_ok=True)

    with open(os.getcwd()+"/"+file_info.file_path, 'wb') as new_file:
        new_file.write(downloaded_file)

    is_okey = input("¿Está la foto bien? 0=No, 1=EsOni, 2=EsXiqui ")

    if is_okey == "0":
        # Responderle que nos envie otra foto
        answer_back = input("Mensaje respuesta: ")
        bot.reply_to(message, answer_back)

    if is_okey == "1":
        bot.reply_to(message, "Cucha!!!!!!!!!!!")
        bot.send_message(message.chat.id,
                         "Perfecto!!\nGran trabajo Naira, estas resultando ser una grán aliada. ¡El Ogro de las montañas no se saldrá con la suya!\n")

        fifth_clue(message)
    if is_okey == "2":
        bot.reply_to(message, "CONSEGUIDOO!!!!!!!!!!!")
        bot.send_message(message.chat.id,
                         "Literalmente ha puesto\n\n... es tan mona en verdad, venga te dejo pasar ... . \n\n¡ Hemos vencido a la astuta Oni !, no se lo va a creer\n¡Lo hemos conseguido!\n")
        bot.send_message(message.chat.id,
                     "Vaya!\n\nhemos cantado victoria antes de tiempo, parece que Oni decidió llevarse el tesoro hace mas de 50 años. \nSolo nos ha dejado una nota")
        opciones_solo_codigo_nota = {"/nota": "Leer nota"}
        bot.send_message(chat_id=message.chat.id,
                         text="¿Quieres ver la nota que nos ha dejado?",
                         reply_markup=makeKeyboard(opciones_solo_codigo_nota),
                         parse_mode='HTML')

@bot.callback_query_handler(func=lambda call: True)
def handle_query(call):
    print(call.data)
    if call.data.startswith("['value'"):
        valueFromCallBack = ast.literal_eval(call.data)[1]
        keyFromCallBack = ast.literal_eval(call.data)[2]
        if keyFromCallBack.startswith("/primero"):
            first_clue(call.message)
        if keyFromCallBack.startswith("/segundo"):
            second_clue(call.message)
        if keyFromCallBack.startswith("/tercero"):
            third_clue(call.message)
        if keyFromCallBack.startswith("/cuarto"):
            fourth_clue(call.message)
        if keyFromCallBack.startswith("/quinto"):
            fifth_clue(call.message)
        if keyFromCallBack.startswith("/nota"):
            read_note(call.message)


def telegram_polling():
    try:
        bot.polling()#none_stop=True, timeout=60)
    except:
        traceback_error_string=traceback.format_exc()
        with open("Error.Log", "a") as myfile:
            myfile.write("\r\n\r\n" + time.strftime("%c")+"\r\n<<ERROR polling>>\r\n" + traceback_error_string + "\r\n<<ERROR polling>>")
        bot.stop_polling()
        #time.sleep(10)
        telegram_polling()


telegram_polling()

